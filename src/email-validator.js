const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

function validate (email) {
  const emailEnding = email.split('@')[1]; // Extract the ending after '@'
  return VALID_EMAIL_ENDINGS.includes(emailEnding);
}

export { validate };
